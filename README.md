# A03 web application Example

A simple Application using Node, Express, BootStrap, EJS, HTML, CSS

## How to use

Open a command window in your c:\44563\A03Gundala folder.

Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node gbapp.js
```

Point your browser to `http://localhost:8081`. 

